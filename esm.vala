namespace ESM
{
  const string EXT_AUTHENTICATION = E.SOURCE_EXTENSION_AUTHENTICATION;
  const string EXT_IDENTITY = E.SOURCE_EXTENSION_MAIL_IDENTITY;
  const string EXT_TRANSPORT = E.SOURCE_EXTENSION_MAIL_TRANSPORT;

  public class Session : Camel.Session
  {
    public E.SourceRegistry registry { get; set construct; }
    public E.SourceCredentialsProvider credentials_provider { get; set construct; }
    private HashTable<string, Camel.Service> service_by_mail;

    private E.Source
    root_source (owned E.Source source)
    {
      string? parent = null;
      do
        {
          parent = source.get_parent ();
          if (parent == null) break;
          source = registry.ref_source (parent);
        } while (true);
      return source;
    }

    private E.Source?
    find_sibling (E.Source source, string? extension)
    {
      var root = root_source (source);

      foreach (var candidate in registry.list_enabled (extension))
        if (root_source (candidate) == root)
          return candidate;

      return null;
    }

    public
    Session (E.SourceRegistry reg)
    {
      Object (registry: reg,
              credentials_provider: new E.SourceCredentialsProvider (reg),
              user_data_dir: E.get_user_data_dir (),
              user_cache_dir: E.get_user_cache_dir ());

      service_by_mail = new HashTable<string, Camel.Service> (str_hash,
                                                              str_equal);

      foreach (var src in registry.list_enabled (EXT_TRANSPORT))
        {
           try
             {
               var ext = src.get_extension (EXT_TRANSPORT);
               var transport = ext as E.SourceMailTransport;
               var service = add_service (src.uid, transport.backend_name,
                                          Camel.ProviderType.TRANSPORT);
               var identity = find_identity (service);
               if (identity != null)
                 service_by_mail.insert (identity.address, service);
             }
           catch (GLib.Error e)
             {
               critical ("%s", e.message);
             }
        }
    }

    public override bool
    authenticate_sync (Camel.Service service,
                       string? mechanism,
                       Cancellable? cancellable)
      throws Error
    {
       if (mechanism == "none")
         mechanism = null;

       Camel.ServiceAuthType? authtype = null;
       if (mechanism != null)
         authtype = Camel.Sasl.authtype (mechanism);

       if (authtype == null)
         throw new Camel.Error.ERROR_GENERIC ("No authentication mechanism");

       if (authtype.need_password)
       {
         var src = registry.ref_source (service.uid);
         E.NamedParameters params;
         credentials_provider.lookup_sync (src, null, out params);
         string? passwd = params.get(E.SOURCE_CREDENTIAL_PASSWORD);
         if (passwd != null)
           service.password = passwd.dup();
       }

       return service.authenticate_sync (mechanism, cancellable) ==
         Camel.AuthenticationResult.ACCEPTED;
    }

    // see also `mail_session_get_oauth2_access_token_sync'
    // in Evolution
    public override bool
    get_oauth2_access_token_sync (Camel.Service service,
                                  out string? access_token,
                                  out int expires_in,
                                  Cancellable? cancellable = null)
      throws Error
    {
      var src = registry.ref_source (service.uid);
      var cred_src = registry.find_extension (src, E.SOURCE_EXTENSION_COLLECTION);
      cred_src = cred_src ?? src;
      return cred_src.get_oauth2_access_token_sync (cancellable,
                                                    out access_token,
                                                    out expires_in);
    }

    public bool
    disconnect_all_services ()
    {
      var ret = true;
      foreach (var service in list_services ())
        {
          try
            {
              if (service.connection_status !=
                  Camel.ServiceConnectionStatus.DISCONNECTED)
                ret &= service.disconnect_sync (true);
            }
          catch (Error e)
            {
              critical (e.message);
              ret = false;
            }
        }
      return ret;
    }

    public Camel.Service?
    get_service_for_address (string address)
    {
      return service_by_mail.get (address);
    }

    public Camel.Transport
    get_transport_for_sender (Camel.InternetAddress sender)
      throws Camel.Error
    {
      string address;
      if (!get_address(sender, 0, out address))
        throw new Camel.Error.ERROR_GENERIC ("Could not parse address");
      var service = get_service_for_address (address);
      if (service == null)
        throw new Camel.Error.ERROR_GENERIC (@"No service for $address");
      return service as Camel.Transport;
    }

    public E.SourceMailIdentity?
    find_identity (Camel.Service service)
    {
      var src = registry.ref_source (service.uid);
      var id_src = find_sibling (src, EXT_IDENTITY);
      if (id_src == null) return null;
      return id_src.get_extension (EXT_IDENTITY)
        as E.SourceMailIdentity;
    }

    public override Camel.Service
    add_service (string uid, string protocol, Camel.ProviderType type)
      throws Error
    {
      var service = base.add_service (uid, protocol, type);

      var ext = E.SourceCamel.get_extension_name (protocol);
      var src = registry.ref_source (uid);
      var auth = registry.find_extension (src, EXT_AUTHENTICATION);
      src = registry.find_extension (src, ext) ?? src;

      src.camel_configure_service (service);

      if (auth != null)
        {
          var auth_ext = auth.get_extension (EXT_AUTHENTICATION)
            as E.SourceAuthentication;
          var auth_uid = auth_ext.dup_proxy_uid ();

          if (uid != null)
            {
              var resolver = registry.ref_source (auth_uid)
                as GLib.ProxyResolver;
              if (resolver.is_supported ())
                service.set_proxy_resolver (resolver);
            }

        }

      E.binding_bind_property (src, "display-name",
                               service, "display-name",
                               GLib.BindingFlags.SYNC_CREATE);

      return service;
    }
  }

  public Camel.MimeMessage
  parse_file (File file) throws Error
  {
    var msg = new Camel.MimeMessage ();
    msg.construct_from_input_stream_sync (file.read (), null);
    return msg;
  }

  extern bool get_address (Camel.InternetAddress addr,
                           int index,
                           out string address);

  void patch_recipients (Camel.MimeMessage msg,
                         string type,
                         Camel.InternetAddress? extra)
  {
    var recipients = msg.get_recipients (type);
    if (recipients != null)
      {
        recipients = recipients.new_clone () as Camel.InternetAddress;
        recipients.cat (extra);
      }
    else
      recipients = extra;
    msg.set_recipients (type, recipients);
  }


  public class Main : Object
  {
    private static string? from_ = null;

    [CCode (array_length = false, array_null_terminated = true)]
    private static string[]? to_ = null;
    [CCode (array_length = false, array_null_terminated = true)]
    private static string[]? cc_ = null;
    [CCode (array_length = false, array_null_terminated = true)]
    private static string[]? bcc_ = null;

    private const OptionEntry[] options =
      {
        { "from", '\0', OptionFlags.NONE, OptionArg.STRING, ref from_,
         "Disregard \"From:\" line and attempt to send as SENDER",
         "SENDER" },
        { "to", '\0', OptionFlags.NONE, OptionArg.STRING_ARRAY, ref to_,
          "Add RECEIVER to \"To:\" line", "RECEIVER..." },
        { "cc", '\0', OptionFlags.NONE, OptionArg.STRING_ARRAY, ref cc_,
          "Add RECEIVER to \"Cc:\" line", "RECEIVER..." },
        { "bcc", '\0', OptionFlags.NONE, OptionArg.STRING_ARRAY, ref bcc_,
          "Add RECEIVER to \"Bcc:\" line", "RECEIVER..." },
        { null }
      };

    private static int
    try_decode(Camel.InternetAddress address, string addr)
      throws Camel.Error
    {
      var length = address.decode(addr);
      if (length < 0)
        {
          var msg = @"Failed to decode \"$addr\"";
          throw new Camel.Error.ERROR_GENERIC (msg);
        }
      return length;
    }

    private static Camel.InternetAddress
    address_from_array (string[]? addrs)
      throws Camel.Error
    {
      var address = new Camel.InternetAddress();
      foreach (string addr in addrs)
        {
          var tmp = new Camel.InternetAddress ();
          if (try_decode (tmp, addr) > 0)
            address.cat (tmp);
        }
      return address;
    }

    public static int
    main (string[] args)
    {
      Intl.setlocale ();

      try
        {
          var opt_context = new OptionContext (null);
          opt_context.set_help_enabled (true);
      opt_context.add_main_entries (options, null);
      opt_context.parse (ref args);
        }
      catch (OptionError e)
        {
          printerr ("""Error: %s
Run '%s --help' to see a list of available options.""",
                    e.message, args[0]);
          return 1;
        }

      Camel.InternetAddress? from = null;
      Camel.InternetAddress to, cc, bcc;

      try
        {
          if (from_ != null)
            {
              from = new Camel.InternetAddress ();
              try_decode (from, from_);
            }
          to = address_from_array (to_);
          cc = address_from_array (cc_);
          bcc = address_from_array (bcc_);
        }
      catch (Error e)
        {
          printerr ("Error: %s", e.message);
          return 1;
        }

      Session session;
      var messages = new List<Camel.MimeMessage>();
      unowned List<Camel.MimeMessage> end = null;

      try
        {
          session = new Session (new E.SourceRegistry.sync ());
          foreach (string arg in args[1:])
            {
              var msg = parse_file (File.new_for_commandline_arg (arg));
              // assert, that we can transport this message
              var sender = from ?? msg.get_from ();
              if (sender == null)
                throw new Camel.Error.ERROR_GENERIC ("Message has no sender");
              session.get_transport_for_sender (sender);

              patch_recipients (msg, "to", to);
              patch_recipients (msg, "cc", cc);
              patch_recipients (msg, "bcc", bcc);

              if (end == null)
                {
                  messages.append(msg);
                  end = messages;
                }
              else
                {
                  end.append (msg);
                  end = end.next;
                }
            }
        }
      catch (Error e)
        {
          printerr ("Error: %s\n", e.message);
          return 1;
        }

      int ret = 0;
      var mainloop = new GLib.MainLoop ();
      try
        {
          foreach (var msg in messages)
            {
              var sender = from ?? msg.get_from ();
              var transport = session.get_transport_for_sender (sender);
              if (transport.connection_status !=
                  Camel.ServiceConnectionStatus.CONNECTED)
                transport.connect_sync ();

              var recipients = new Camel.InternetAddress ();
              recipients.cat (msg.get_recipients ("to"));
              recipients.cat (msg.get_recipients ("cc"));
              recipients.cat (msg.get_recipients ("bcc"));

              transport.send_to.begin (msg, sender, recipients,
                                       Priority.DEFAULT,
                                       null,
                                       (obj, result) =>
                 {
                   try
                     {
                       bool saved;
                       transport.send_to.end (result, out saved);
                     }
                   catch (Error e)
                     {
                       printerr ("Error while sending: %s\n", e.message);
                       ret = 1;
                     }
                   messages.remove (msg);
                   if (messages == null)
                     mainloop.quit ();
                 });
            }
          mainloop.run ();
        }
      catch (Error e)
        {
          printerr ("Error: %s\n", e.message);
          ret = 1;
        }
      finally
        {
          session.disconnect_all_services ();
        }

      return ret;
    }
  }
}
