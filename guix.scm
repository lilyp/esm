(use-modules (guix packages)
             (guix gexp)
             (guix git-download)
             (guix licenses)
             (guix build-system meson)
             (gnu packages glib)
             (gnu packages gnome)
             (gnu packages pkg-config))

(define %source-dir (dirname (current-filename)))
(define %select? (git-predicate %source-dir))

(package
 (name "esm")
 (version "0.1")
 (source (local-file %source-dir #:recursive? #t #:select? %select?))
 (build-system meson-build-system)
 (arguments
  `(#:glib-or-gtk? #t
    #:phases
    (modify-phases %standard-phases
      ;; Application is not graphical
      (delete 'generate-gdk-pixbuf-loaders-cache-file))))
 (inputs (list evolution-data-server glib))
 (native-inputs (list pkg-config vala))
 (home-page "https://gitlab.gnome.org/lilyp/esm")
 (synopsis "Send mboxes over Evolution")
 (description "Evolution Sendmail (ESM) is a command line program to send
mail (encoded as mbox) from the command line with a mail account known to your
local evolution-data-server.")
 (license lgpl2.1+))
