#include <glib.h>
#include <camel/camel.h>

gboolean
esm_get_address (CamelInternetAddress *address,
                 gint index,
                 char **out_address)
{
  const char *name, *address_;
  if (camel_internet_address_get (address, 0, &name, &address_))
    {
      *out_address = g_strdup (address_);
      return TRUE;
    }
  else
    return FALSE;
}
